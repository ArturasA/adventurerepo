CREATE VIEW dbo.vCurrentCurrencyRate
AS
WITH maxDate AS (
SELECT CurrencyKey, MAX(Date) Date FROM dbo.FactCurrencyRate GROUP BY CurrencyKey)

SELECT 
C.CurrencyAlternateKey, C.CurrencyName, AverageRate, EndOfDayRate, MD.Date
FROM DimCurrency C
INNER JOIN FactCurrencyRate CR ON C.CurrencyKey = CR.CurrencyKey
INNER JOIN maxDate MD on MD.CurrencyKey = CR.CurrencyKey AND MD.Date = CR.Date
GO